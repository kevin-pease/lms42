from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import TextField, SubmitField, PasswordField, SelectField, DecimalField, TextAreaField, BooleanField, FieldList, FormField, validators

# poetry install && poetry run flask run --reload
app = Flask(__name__)
app.secret_key = "Secret"
app.config["TEMPLATES_AUTO_RELOAD"] = True

class MyForm(FlaskForm):
    email = TextField('Email address', validators=[validators.InputRequired(), validators.Email()])
    password = PasswordField('Password', validators=[validators.Length(6)])
    confirm  = PasswordField('Password (again)', validators=[validators.EqualTo('password', message='Passwords must match')])

    person_type = SelectField('What are you, exactly?', choices=[('dwarf','A dwarf'),('elf','An elf'),('ai','An AI'),('microbe','A microbe'),('fictional','A fictional character')], validators=[validators.InputRequired()])
    prime_age = DecimalField('At what age will you become enlightened?', validators=[validators.NumberRange(23, 123)])
    dedication = TextAreaField('What have you done for the Flying Spaghetti Monster lately?', validators=[validators.Length(80)])
    priority = RadioField('Priority', [validators.InputRequired()], choices=['low', 'medium', 'high', 'critical'])  
    sense = RadioField('Does this make any sense?', [validators.InputRequired()], choices=['No', "Define 'sense'", 'Only if you look at it backwards', 'Joshua tree'])

    tos = BooleanField("I pledge my allegiance to the flag.", validators=[])
    submit = SubmitField('Submit')

@app.route('/', methods=['GET', 'POST'])
def index():
    form = MyForm()
    if form.validate_on_submit():
        return "Submitted!"

    return render_template('generic-form.html', form=form)

