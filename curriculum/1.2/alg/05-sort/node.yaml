name: Sorting
goals:
    alg_impl: 1
days: 1
assignment1:
    Assignment:
        - |
            Computers often need to put information in order. Putting a list of strings in alphabetic order, or a list of numbers in ascending order, for instance. Using the right algorithms, computers can do this really fast. Even when many millions of items need to be sorted! Using the wrong algorithms, sorting millions of items will not finish before the sun collapses, in about 5 billion years.
    Give it a try!:
        -
            text: |
                Alright, so without any further info, we challenge you to come up with your own custom sort algorithm! Implement it in the `custom_sort(data)` function in the provided `sort.py`. The function gets a `list` as its only argument, and it should shuffle the elements in that list around such that when the function returns, the items are sorted.

                Take at least 30 minutes (and at most, say, 90 minutes) to think about and implement the best sorting algorithm you can come up with. It helps to try manually sorting physical objects first. A deck of playing cards should be available in the classroom. 

                *Yes, you could easily peek at some information about sorting algorithms (such as provided below). But by doing that you would miss out on your one opportunity in life to come up with a sorting algorithm without borrowing ideas from others. In case you don't care about that: thinking about the problem yourself first really helps you to understand the algorithms that are presented below.*

            weight: 0.5
            bonus: 0.25
            0: Pretty far from anything that might work.
            1: It sorts some small arrays correctly, or is close to something workable.
            2: It sorts most small arrays correctly.
            3: It sorts arrays of hundreds correctly!
            4: It happily sorts arrays of millions!
    Selection sort:
        - Selection sort is one of the simpler (and slower) sort algorithms. Perhaps you have tried to implement something like this above? Let's have a look!
        -
            link: https://www.youtube.com/watch?v=3hH8kTHFw2A
            title: CS50 - Selection Sort
        -
            link: https://math.hws.edu/eck/js/sorting/xSortLab.html
            title: xSortLab
            description: A step by step demonstration. Note that they work by continuously searching for the largest element and putting it at the back, instead of putting with the smallest elements at the front. Both ways are fine.
        -
            text: |
                Implement selection sort in the `selection_sort(data)` function. Make sure the provided unit test passes, that you're indeed using the right algorithm and that you can fully explain what is happening.
            
                *Yes, you can copy/paste this from any number of places. But please don't. Implementing this yourself really is a great way to become a better programmer.*
            code: 0.3
            0: It doesn't sort.
            1: It (kind of) sorts, but using the wrong algorithm.
            2: Similar but different algorithm, vaguely explained.
            3: Almost right algorithm, properly explained, unit tests (mostly) pass.
            4: Right algorithm, properly explained, unit tests pass.
    Merge sort:
        - Merge sort is a *recursive* algorithm. As we'll discover, it's pretty speedy even for very large lists.
        -
            link: https://www.youtube.com/watch?v=Ns7tGNbtvV4
            title: CS50 - Merge Sort
        -
            link: https://math.hws.edu/eck/js/sorting/xSortLab.html
            title: xSortLab
            description: The same demo, but select the 'merge sort' algorithm this time.
        -
            text: |
                Implement merge sort in the `merge_sort(data)` function. Make sure the provided unit test passes, that you're indeed using the right algorithm and that you can fully explain what is happening.
            
                *Yes, you can copy/paste this from any number of places. But please don't. Implementing this yourself really is a great way to become a better programmer.*
            code: 0.3
            0: It doesn't sort.
            1: It (kind of) sorts, but using the wrong algorithm.
            2: Similar but different algorithm, vaguely explained.
            3: Almost right algorithm, properly explained, unit tests (mostly) pass.
            4: Right algorithm, properly explained, unit tests pass.
    Quick sort:
        - The last algorithm we'll cover today is quick sort. Like merge sort, it is recursive, but its implementations are usually a bit... quicker. Hence te name.
        -
            link: https://www.youtube.com/watch?v=kUon6854joI
            title: Udacity - Quicksort
        -
            link: https://math.hws.edu/eck/js/sorting/xSortLab.html
            title: xSortLab
            description: Again the same demo, but pick 'quick sort' this time.
        -
            text: |
                Implement quick sort in the `quick_sort(data)` function. Make sure the provided unit test passes, that you're indeed using the right algorithm and that you can fully explain what is happening.
            
                *Yes, you can copy/paste this from any number of places. But please don't. Implementing this yourself really is a great way to become a better programmer.*
            code: 0.3
            0: It doesn't sort.
            1: It (kind of) sorts, but using the wrong algorithm.
            2: Similar but different algorithm, vaguely explained.
            3: Almost right algorithm, properly explained, unit tests (mostly) pass.
            4: Right algorithm, properly explained, unit tests pass.
    Performance comparison:
        -
            text: For each of the three algorithm implementations (or four, if you want your custom algorithm to participate) run experiments to find out *approximately* how many numbers in a list it can sort in about one second. Put our answers in the provided `performance.txt` file.
            weight: 0.5
            0: Nothing there.
            2: Experiments mostly done, but only for 1 or 2 algorithms.
            4: Experiments done right.
