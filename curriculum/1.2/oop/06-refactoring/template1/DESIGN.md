## *Mens erger je niet* (v2)

```plantuml
abstract class Thingy {
    -foo: int
    +{abstract}launch_missiles(): boolean
}

class Gadget {
    -{static}panic(msg: str)
}

Gadget --> "42" Thingy: thingies

note left: All this is nonsense.
```