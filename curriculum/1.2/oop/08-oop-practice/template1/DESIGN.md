# Game library

## Class diagram

```plantuml
package library {
    Game o--> "0..*" Entity
    Entity <|-- Rectangle
    Entity <|-- Circle
    Entity <|-- Text
    Entity <|-- Image
}

package goodsie {
    
    Rectangle <|-- Wall
    Circle <|-- Hero
    Game <|-- Goodsie
    Image <|-- Background
    Text <|-- StartText
}
```