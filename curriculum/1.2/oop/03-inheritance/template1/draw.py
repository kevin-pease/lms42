import pygame

MAX_WIDTH = 800
MAX_HEIGHT = 600
BACKGROUND_COLOR = (255,255,255)

def draw_shapes(shapes):
  # Create a pygame window
  screen = pygame.display.set_mode((MAX_WIDTH, MAX_HEIGHT))
  pygame.display.set_caption('Inheritance')
  screen.fill(BACKGROUND_COLOR)

  # Draw stuff here..
  # For example a blue circle:
  pygame.draw.circle(screen, (0,0,255), (100,100), 15, 1)

  # Show the output
  pygame.display.flip()

  # Keep the program running until the user closes the window
  running = True
  while running:
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        running = False
