name: Amaze
description: Add a feature to a pre-existing code base.
goals:
    legacy_analyze: 1
    legacy_impl: 1
resources:
    -
        link: https://www.youtube.com/watch?v=wN4ZuGruiNw
        title: CUSEC 2016 – How to Read Unfamiliar Code by Josh Matthews - Video
        info: |
            This is a great talk on how not to panic when studying a large unknown code base. Unfortunately, the slides are not visible in the video, but they're available in the next link.
    -
        link: https://www.joshmatthews.net/cusec16/unfamiliar.html
        title: CUSEC 2016 – How to Read Unfamiliar Code by Josh Matthews - Slides
    -
        title: Contributing to Open Source for the first time
        link: https://www.youtube.com/watch?v=c6b6B9oN4Vg
        info: A 18m video on how to contribute to an Open Source project using GitHub.
    -
        title: Open Source Guides - How to Contribute to Open Source
        link: https://opensource.guide/how-to-contribute/
        info: This is a great guide, mostly about the social aspects of contributing. Highly recommended!
intro:
    About the exam: |
        <div class="notification">
            The exam for this module consists of a project that requires you to implement something based on your own idea. We recommend that you come up with an idea in advance. The project requirements are available at any time. You may also want to start thinking about what you'd like to contribute to our LMS, which is the next assignment.
        </div>

assignment:
    - |
        Let's add a feature to an actual Open Source project!

    -
        link: https://repo.hboictlab.nl/template/34094632
        title: "Saxion Repo: Amaze File Manager"
        info: Use this page to create your own fork of the *Amaze File Manager* on GitLab.

    - |
        Browse around the source code. It's fairly typical project: code quality is mostly fine but not stellar, documentation is mostly absent, there is some automated testing but it covers only a small part of the functionality (which is especially common for front-ends).

    - |
        Clone the source code to your computer and try to get it to compile and run from Android Studio.

    -
        must: true
        text: Create a new *feature branch* (named `jpg-to-png`) which you'll use to develop the feature described in the next objective.

    -
        ^merge: feature
        weight: 2
        code: 0
        text: |
            When long-tapping a `.jpg` file in Amaze, an additional option named *Convert to PNG* should be shown in the context. Tapping that option should attempt to convert the original file into a PNG file, and write it in the same directory and with the same file name but with the `.png` extension instead of `.jpg`.

            After successful conversion, *PNG created* should be shown as a Toast message. On any error (such a JPG load failure, or permission denied while creating the PNG file), a *Failed to create PNG* Toast should be shown.
        
    -
        1: Two distinct obvious coding style errors.
        2: One obvious coding style error - won't be accepted.
        3: The maintainers might accept this.
        4: Exactly like the solution diff, or better.
        text: |
            Make sure your changes match the project's coding standards.

    -
        1: 2 of the below.
        2: 3 of the below.
        3: 4 of the below.
        4: Single well-named patch (or multiple well-organized patches), proper copyright, clear and concise description, well-named origin branch, PR to the right branch.
        text: |
            Create a *merge request* for your changes to the `main` branch of your fork. (Normally, you'd create *merge requests* to the *upstream* repository, owned by the maintainers of the official version, but in this case we want you to keep your changes private, so as not to spoil the fun for other students.) You *can* of course have a peek at real-life *pull requests* (the GitHub equivalent of GitLab's *merge requests*) for the [official Amaze repository](https://github.com/TeamAmaze/AmazeFileManager/pulls?q=is%3Apr) for inspiration.

            Make sure that you establish your copyright over your changes in at least the file that you changed most.

