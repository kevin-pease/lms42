/*
 * Provided here is just some DOM creation code to get you started.
 * It is *not* part of the assignment solution, but just a demo.
 *
 * You'll want to create some convenient abstraction for reducing
 * the amount of code needed for these types of things.
 *
 * Also, one of your first steps should be to try to get data from/to
 * the API using fetch(). You'll want to create an abstraction
 * for that as well.
 */

let mainE = document.createElement('main');
document.body.appendChild(mainE);

function showAddCard() {
  let cardE = document.createElement('section');
  cardE.className = "card";
  cardE.innerText = "This is where the magic should be happening. ";

  let buttonE = document.createElement('button');
  buttonE.innerText = "Click me";
  buttonE.addEventListener('click', showInputCard);
  cardE.appendChild(buttonE);

  mainE.appendChild(cardE);
}

function showInputCard() {
  let cardE = document.createElement('section');
  cardE.className = "card";

  let h2 = document.createElement('h2');
  cardE.appendChild(h2);
  
  let inputE = document.createElement('input');
  inputE.type = "text";
  inputE.addEventListener('input', function() {
    h2.innerText = this.value;
  });
  cardE.appendChild(inputE);
  
  mainE.appendChild(cardE);
}

showAddCard();
