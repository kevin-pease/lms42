- |
    Programming is a little bit like cooking. You have ingredients (variables) and a recipe (algorithm). In this first assignment we will build a simple cookbook in Python.

    Start with the tasks below before you jump into implementing the required features.
    
-   Tasks:
    -   Check your setup: |
            At this point you should have Python 3 installed on your laptop. You can verify wether you have it installed by running the following command:
            ```
            $ python --version
            Python 3.9.0
            ```
            If you do not have Python installed please install the latest version.
    -   Running Python code: |
            Python is a interpreted language which means that the code you type does not need to be compiled before it can be run. The Python language has an interpreter which translates the instructions in your code to the tasks the computer needs to execute. Python has an interactive shell in which the instructions for the interpreter can be input directly. To use the interactive Python shell (also sometimes called a “Python REPL”) just type _python3_ in the terminal.

            ```
            $ python
            Python 3.9.0 (default, Nov  4 2020, 12:08:54) 
            [Clang 11.0.3 (clang-1103.0.32.62)] on darwin
            Type "help", "copyright", "credits" or "license" for more information.
            >>> print("Hello World!")
            Hello World!
            >>>
            ```

            In the example above the Python shell is started and a print statement has been typed. As you can see the shell directly executes the print statement after `enter` has been pressed. Try it for yourself!

            It is also possible to store the instructions in a file which you pass to Python. For example create a file called _main.py_ and save the print statement in this file. Using the command below you can execute the instructions in the file. Give it a try!

            ```
            $ python main.py
            Hello World!
            $
            ```
            
            From now on we write our code in _.py_ files. Of course you can always use the Python shell to quickly test a small piece of code.

        Prototyping in the Python shell: |
            To try out a small piece of code you can use the Python REPL by simply typing _python_ from the command line. A better version of the REPL has been implemented in _bpython_. This is a small command line application which adds auto-completion to the Python REPL. It is highly recommended you install bpython to make prototyping your Python code a breeze.
            
-   Features:
    -   Handle input and output:
        - 
            link: https://www.youtube.com/watch?v=ZEuQypLGUgw
            title: Python3 Beginner Tutorial 3 - Input & Output
            info: This is an introduction to Python Input & Output. 
        -   
            link: https://www.geeksforgeeks.org/formatted-string-literals-f-strings-python/
            title: f-strings in Python
            info: One of the string formatting methods in Python3, which allows multiple substitutions and value formatting. This method lets us insert elements on different positions within a string.
        -   text: |
                The application prints a welcome message and asks for the name of a recipe:
                ```
                *** Welcome to the programmer's cookbook ***
                We are going to create a recipe for omelettes.
                Give your recipe a name (for example 'Awesome Omelette'): My Omelette
                Your recipe is named: My Omelette
                ```
                - The application prints a welcome message.
                - The application asks the name of the recipe.
                - The application stores the name of the recipe in a variable.
            notes:
                - Print fixed text to console
                - Read input from console
                - Print fixed text with value of variable
            ^merge: feature
            code: 0

    -   Store different ingredients:
        - 
            link: https://development.robinwinslow.uk/2014/01/05/summary-of-python-code-style-conventions/
            title: A summary of python code style conventions
            info: You should make an effort to internalize these best-practice conventions. Here is a quick reference for anyone who might find it useful.
        -   
            link: https://www.geeksforgeeks.org/formatted-string-literals-f-strings-python/
            title: f-strings in Python
            info: One of the string formatting methods in Python3, which allows multiple substitutions and value formatting. This method lets us insert elements on different positions within a string.
        -   text: |
                The application asks to fill in the ingredients:
                ```
                *** Welcome to the programmer's cookbook ***
                We are going to create a recipe for omelettes.
                Give your recipe a name (for example 'Awesome Omelette'): My Omelette
                Your recipe is named: My Omelette
                For a 2 person omelette, how many eggs do you need: 3
                For a 2 person omelette, how many tablespoons of milk do you need: 1
                For a 2 person omelette, how much butter do you need (grams): 20
                All done!
                ```
                - The application asks to input the amount per ingredient and stores the values in separate variables.
                - The application stores the amounts in the proper data types (all values are numbers).
                - The application uses a 'constant' for the number of people in the standard recipe (which is 2). Although Python does not support the concept of constants (variables for which the value can not be changed once they have been declared) it is a best practice in Python to name these variables in CAPITAL casing.
            ^merge: feature
            code: 0
    -   Cast input to the proper types:
        -
            link: https://pythonexamples.org/python-type-casting/
            title: Python Type Casting
            info: In Python, Type Casting is a process in which we convert a variable of one type to another. The built-in functions int(), float() and str() can be used for typecasting.
        -
            link: https://www.geeksforgeeks.org/python-string-isdigit-application/
            title: Python String isdigit() and its application
            info: The isdigit() method returns “True” if all characters in the string are digits, Otherwise, It returns “False”.
        -
            link: https://pythonbasics.org/if-statements/
            title: If Statements Explained
            info: An if statement evaluates data (a condition) and makes a choice.
        -   text: |
                The application asks to fill in the ingredients:
                ```
                *** Welcome to the programmer's cookbook ***
                We are going to create a recipe for omelettes.
                Give your recipe a name (for example 'Awesome Omelette'): My Omelette
                Your recipe is named: My Omelette
                For a 2 person omelette, how many eggs do you need: abc
                Input 'abc' is not a valid number. We will set the number of eggs to 3.
                For a 2 person meal, how many tablespoons of milk do you need:
                Input '' is not a valid number. We will set the number of tablespoons to 2.
                For a 2 person meal, how much butter do you need (grams): a lot
                Input 'a lot' is not a valid number. We will set the amount of butter to 20 grams.
                All done!
                ```
                - The application asks to input the amount per ingredient. Any value a users inputs in the application is of type string. In order to properly store and process the variable the inputted string needs to be converted to the correct type. Converting the type of a variable is called casting.
                - Use the python isdigit() function to check whether the inputted string is a digit. If this is the case then the input should be casted to the correct type. If not then the default value should be used.
            ^merge: feature
            code: 0

    -   Scale the recipe:
        - 
            link: https://www.youtube.com/watch?v=Aj8FQRIHJSc&list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-&index=7
            title: Socratica - Arithmetic in Python
            info: A video explaining typing and how arithmetic works in Python.
        -   text: |
                The application asks to fill in the number of mouths to feed and scales the ingredients accordingly:
                ```
                *** Welcome to the programmer's cookbook ***
                We are going to create a recipe for omelettes.
                Give your recipe a name (for example 'Awesome Omelette'): My Omelette
                Your recipe is named: My Omelette
                For a 2 person omelette, how many eggs do you need: 3
                For a 2 person omelette, how many tablespoons of milk do you need: 1
                For a 2 person omelette, how much butter do you need (grams): 20
                All done!
                *** Let's get cooking ***
                How many omelettes do you want to make?
                Fill in the number of mouths to feed:
                Input '' is not a valid number. We will set the number of people to 3.
                For 3 people you need:
                 * 4.5 eggs
                 * 1.5 tbsp of milk
                 * 30 grams of butter
                Instructions:
                 1. Whisk the eggs with the milk, season with salt and pepper.
                 2. Coat a pan with butter and heat over medium heat.
                 3. Once the pan is hot, pour in the mixture.
                 4. When the egg is cooked, fold the omelette in half with a spatula.
                 5. Let the folded omelette brown slightly and serve it on a plate.
                Enjoy!
                ```
                - The application asks to input the amount of people to make omelettes for. If an invalid number is supplied then the default will be set to 3.
                - The application computes and prints the amount of eggs required for the recipe. Note that this may be a fraction.
                - The application computes and prints the amount of tablespoons of milk required for the recipe. Note that this may be a fraction.
                - The application computes and prints the amount of butter required for the recipe. Note that this should be a whole number (round to nearest integer).
                - The application prints the instructions to make an omelette. Use a multi-line string to print all instructions using one print statement.
            ^merge: feature
            code: 0
-   Non-functional requirements:
    -
        title: Writing expressive code
        link: https://development.robinwinslow.uk/2013/11/22/expressive-coding/
        info: Writing expressive code may help future coders to understand what’s going on. It may even help you in the future. But it may also help you simply to understand the problem.
    -
        title: A Guide for Naming Things in Programming
        link: https://levelup.gitconnected.com/a-guide-for-naming-things-in-programming-2dc2d74879f8
        info: Set of guidelines you can follow while naming things in programming
    -   Code quality:
        -
            link: https://development.robinwinslow.uk/2013/11/22/expressive-coding/
            title: Writing expressive code
            info: Writing expressive code may help future coders to understand what’s going on. It may even help you in the future. But it may also help you simply to understand the problem.
        -   text: Your code should be easy to read, using proper naming, comments and white space.
            ^merge: codequality
            weight: 1.5
