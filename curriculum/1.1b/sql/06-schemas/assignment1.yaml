- |
      In this assignment you are provided with an ERD of a use case. Your assignment (should you choose to accept it) is to create a database based on the ERD. In addition we ask you to extend the model with additional data which should also be inserted into the database.
      
      Good luck!

- Assignment:    
    - Create a database from ERD:
        - 
            link: https://www.sqlshack.com/learn-sql-naming-conventions/
            title: "Learn SQL: Naming Conventions"
            info: A naming convention is a set of unwritten rules you should use if you want to increase the readability of the whole data model.
        -   text: |
                We are going to implement a database for storing data about a curriculum of a programme. For populating our database we will use data of our own programme.
                
                ![](sql-schema-curriculum.png)

                Included in the assignment is a picture of the ERD - a logical data model - for this use case. Create a database based on this diagram:
                 - Add the required tables into the database, including the required columns. This logical data model does not contain (primary and foreign) keys, you should create them for your database where applicable.
                 - Give the columns the proper types and enforce that required columns are not NULL. Attributes in the ERD marked with "(Optional)" are nullable meaning that they do not require a value.
                 - Adhere to the SQL naming conventions when creating your database.
                
            0: No tables.
            1: Two correct query.
            2: Four correct query.
            3: Six correct query.
            4: All tables are correct.

    - Mind the relationships:
        -
          link: https://www.youtube.com/watch?v=KonhU0IiVgI
          title: On ER to Relational schema mapping
          info: In this video the mapping between the different relationships in an ERD to a SQL schema is explained.    
        - 
            link: https://www.sqlite.org/foreignkeys.html
            title: SQLite foreign key support
            info: The first section introduces the concept of an SQL foreign key by example and defines the terminology used.
        -
            text: |
                The ERD design does not (directly) include the required foreign keys for the database. Study the relationships between the different entities in the diagram and update your database accordingly. If you need to add additional columns (not present in the ERD) to your database, please do so.

            0: No relationships have been implemented.
            1: Some relationships are correctly implemented.
            2: Half relationships are correctly implemented.
            3: Almost all relationships are correctly implemented.
            4: All relationships are correctly implemented.

    - Populate the database:
        -
            text: |
                For the data for our database we will look at our own programme. Insert into the database a programme named "Associate degree Software Engineering" with a duration of "2" years. Insert the modules for the programme. The following modules should be added to the database:

                1. Title: Coding in Python. Description: Python is a programming language that is relatively easy to use for beginners, but commonly used by experts as well. It is a general purpose language, meaning it can be used to create many types of computer programs. It is often used for quickly automating tasks, data analysis, artificial intelligence and for creating web applications. In this module, we'll learn the basics of imperative programming using Python.
                2. Title: Static web. Description: At its core, the world wide web consists of a collection of documents that link to each other. The documents are written using HTML and styled using CSS. Both are languages intended to be understood by computers, but are not programming languages. We will learn these languages to be able to create decent-looking web pages. Later on, we'll build on this knowledge to create web applications.
                3. Title: SQL. Description: Applications usually work on (large amounts of) information. Such information is often stored in a database that is controlled by a database management system. The system we'll be working with is SQLite. Like other well-known database systems (such as Oracle, MySQL and PostgreSQL) one can interact with it using the SQL computer language in order to store and retrieve data. We will be learning how to represent real-world objects as an SQL database, and how to efficiently generate all kinds of useful reports from this data.

                Insert the data using the SQL INSERT command. Save your commands in *commands.sql*.
            
            1: No data has been imported. 
            2: Some data has been imported.
            3: All data has been imported but not not all relations are correct.
            4: All data and relationships are correctly imported.

    - Import data into the database:
        - 
          link: https://www.youtube.com/watch?v=nEndOUQFaOI
          title: SQL Into - How to Copy Table Data with Select Into Statement
          info: Discusses the SELECT INTO and INSERT INTO SELECT statements. Be sure to watch the entire clip for the trick at the end.
        -
            text: |
                When importing data into a database a Comma Separated Values (CSV) format is often used. This is a simple text file in which the different values (for one row in the database) are separated by a comma (or in our case a semicolon ';'). The first line in the CSV file is the header which defines the name for all the values.

                For importing the lessons a csv file is included (*import-lessons.csv*). In order to properly import the data into the database the following strategy is suggested:

                1. Create a temporary table (based on the lessons table) for which the data can be imported. Call this table *import_lessons* and be sure that the names of the columns are exactly the same as the headers defined in the CSV file.
                2. Use the SQLiteStudio import function to import this data into the database. Take note that the provided CSV files are semicolon `;` separated.
                3. The temporary table now contains all the lessons for all the modules. Use a SELECT INTO or INSERT INTO SELECT to insert the data in the proper format into the lessons table. Save these queries in the *commands.sql* file.
                
                A few hints on the SQLiteStudio import function:

                - In case SQLiteStudio complains that no import plugins are available, make sure you have the Manjaro  *sqlitestudio-plugins* package installed.
                - In the import wizard, under *Data source options* check the *First line represents CSV column names* option, and to select `; (semicolon)` as the *Field separator*. (You may need to make the wizard window a bit larger to see the later option.)
                - In case your column names don't match the names in the `.csv` files, feel free to modify the headers in these files. 
                - If you have extra fields that are not nullable, you can temporarily make them nullable. Values can be set using an `update` query.
            
            1: No data has been imported. 
            2: Some data has been imported.
            3: All data has been imported but not not all relations are correct.
            4: All data and relationships are correctly imported.



    - Extend the database:
        -
            link: https://www.youtube.com/watch?v=xoTyrdT9SZI
            title: Basic Concept of Database Normalization - Simple Explanation for Beginners
            info: This video covers the basics of database normalization. What is the problem without normalization and how normalization solves the problem.
        - 
            text: |
                Included in the assignment is a file called 'grades.csv'. This file contains the grades from certain students for the exams for some of the modules. Your task is to store this information into the database. Note that the data in the current format cannot directly be import into the database. You should create the required tables and relationships for properly storing the data in the database. 
                Be sure to normalize the data and extend your database in order to properly store the data. You can assume that all students have enrolled into the programme on 01/02/2021 and that none of them have yet finished. For the student number you can choose any format you want.

                For importing the data use the strategy as described before (first directly import the CSV in a temporary table called *import_grades*). Don't forget to save these queries into the *commands.sql* file.

            0: Database was not extended.
            1: Incorrect extension of database with no import.
            2: Incorrect extension of database with some incorrect data import.
            3: Correct extension of database but not all the data or relations are imported correct.
            4: Correct extension of database including correct data and relationships.
            weight: 2.0

    - Viewing student progress:
        -
            text: |
                In order to check on the user's progress it would useful to have a view (called *v_student_progress*) that queries the grades to determine whether a student has passed a module. The view should display the student's name, the total amount of credits it has collected and the percentage of the collected credits compared to the total amount of credits of the programme. Don't forget to save these queries into the *commands.sql* file.
                
                Note that in order to compute percentage, the total amount of credits for the programme (which is 120) should be stored in the database. Also the amount of credits for each module should be stored into the database (all modules are 5 credits).

            0: No view created.
            2: A view with based on an incorrect has been created.
            4: The created view is correct.

    - One more thing...:
        -
            text: |
                There is one part of the database that can still be normalized. Can you spot it? If so update the database accordingly.

            0: Not spotted.
            2: Spotted something but not what we are looking for.
            4: Spotted and correctly solved.
            weight: 0.5





