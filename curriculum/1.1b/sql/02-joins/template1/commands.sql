-- Joining tables:
-- 1. Display the name and surname of the pet owners including the name of their pets. 


-- 2. Display the name and surname of all pet owners (including the owners that have no pets). Include the name of their pets (if applicable).


-- 3. Display the pet name, date and procedure type of a pets procedure history.


-- 4. Display the owner name, pet name and procedure date, type and subcode of all the procedures of (known) pets.

-- Join and filter:
-- 1. Display the name and surname of the pet owners that have a cat as pet. Include the name and kind of their pet(s).


-- 2. Display all cities that have female parrots without duplicates.


-- 3. Display the pet name, kind and procedure date of the last 25 procedures that have been done.


-- 4. Display the name, gender and age of the cat with the most expensive procedure. Also display the procedure type and price.


-- 5. Display the different types of procedures the pet of owner 'Max Smith' has had. Display the owner (first and last) name, the pet name and kind. Your results should not contain duplicates.


-- 6. Display the name and kind of all pets that have had a procedure that cost more than 100 euros. Also display the address (street and city) where the pet lives. Sort the results by pet name.


-- Start the count:
-- 1. Count the number of owners in the database.


-- 2. Count the number of owners in the database that have no pets.


-- 3. Count the number of owners that have the name 'Charles' and have a pet cat. (Make sure you count each Charles only once, even he has more than one cat.)


-- 4. Count the number of pets in the database that have not endured a procedure yet.