Introduction:
    - |
        In this assignment you are provided with a use case for which you will have to derive a data model. In addition you will have to derive a data model for a use case of your own.

"Use case: stackoverflow.com":
    - |
        We want to build an application that stores questions and answers much like stackoverflow.com. Users can register in the application with their name and email address. Users can ask questions. A question has a title and a description explaining the question. Users can also respond to a question with an answer. Aside from responding to a question a user can up vote or down vote the response. Questions can be categorized in one or more categories, each containing a label and a description. Every time a question is viewed by a user it will be recorded (with a timestamp) so that a list of popular (most viewed) questions can be made. It should be possible to order the responses to a question by the time they were submitted.

    - Derive the entities from the use case:
        - 
            link: https://www.youtube.com/watch?v=PESLfjlWVSU
            title: Identifying Entities
            info: A short video about a simple technique to identify which tables and columns you may want to create based on a case description.
        -   text: |
                List all the entities (tables) and attributes (columns) that have been mentioned in the use case in `entities.txt`.

            0: Barely anything.
            2: Largely complete and some spurious.
            4: Complete list, no spurious entities.
            map:
                datamodeling: 1
                foreignkeys: 0
            
    - Design a logical data model:
        - 
            link: https://www.visual-paradigm.com/guide/data-modeling/what-is-entity-relationship-diagram/#erd-data-models-logical
            title: "What is Entity Relationship Diagram (ERD)? - Logical data model"
            info: Database is absolutely an integral part of software systems. To fully utilize ER Diagram in database engineering guarantees you to produce high-quality database design to use in database creation, management, and maintenance. An ER model also provides a means for communication.
        -
            text: |
                Create an Entity Relation Diagram (ERD) for your data model. Draw the entities and their relationships in an *logical data model*. Don't let tools distract you: just use pen and paper! Yes, you'll probably need to start over 1 or 2 times, bit still this will save you time.

                For each connection in your diagram, add the right cardinality symbol (or a number) on both sides.

                After you're done with your drawing, just take a photo of it and put it in your submission.

            2: Needs more work before this can be used.
            3: Some small issues, but good enough to start creating a database.
            4: Perfect.
            map:
                datamodeling: 2
                foreignkeys: 0

    - Design a physical data model:
        - 
            link: https://www.visual-paradigm.com/guide/data-modeling/what-is-entity-relationship-diagram/#erd-data-models-physical
            title: "What is Entity Relationship Diagram (ERD)? - Physical data model"
            info: Database is absolutely an integral part of software systems. To fully utilize ER Diagram in database engineering guarantees you to produce high-quality database design to use in database creation, management, and maintenance. An ER model also provides a means for communication.
        - 
            link: https://www.essentialsql.com/what-is-the-difference-between-a-primary-key-and-a-foreign-key/
            title: Foreign and Primary Key Differences (Visually Explained)
            info: This article will teach you the difference between a primary key and a foreign key. This article will also teach you why both of these keys are important when it comes to the maintenance of a relational database structure.

        -
            text: |
                Create a *physical data model* for this use case. The physical data model should describe the (primary and foreign) keys and the attribute types. For a primary key use the abbreviation "PK" and for a foreign key use the abbreviation "FK".

                Be sure to apply the correct naming conventions (whitespace is not allowed) in your ERD.

                After you're done with your drawing, just take a photo of it and put it in your submission.

            2: Needs more work before this can be used.
            3: Some small issues, but good enough to start creating a database.
            4: Perfect.
            weight: 2

"Use case: twitter.com":
    - |
        In this use case we want to derive a physical data model for twitter. For our data model we would like keep track of users, tweets and hashtags. Our users have a handle (for example @TimothySealy), a small bio and a date that they have joined. A user can follow another user. A user can create a new tweet. An existing tweet can be retweeted or liked. The time at which the tweet was retweeted or liked is important in order to determine whether a tweet is going viral (for example many retweets and/or likes in a certain time period). A tweet can have zero or more hashtags. Hashtags only have a label.

    - Design a physical data model:
        -
            text: |
                Create a physical data model for this use case.

                After you're done with your drawing, just take a photo of it and put it in your submission.
                
            2: Needs a bit more work.
            3: One or two minor issues.
            4: Perfect.
            weight: 3

"Use case: An application of your choice":
    - Choose a proper use case:
        must: true
        text: The diagram has at least six entities, twelve attributes and eight relations. 
    - Design a physical data model:
        -
            text: |
                Create another ERD - a physical data model - complete with cardinalities for a (web) application of your choice. Of course, you need to make sure your choice is not stackoverflow.com, or twitter.com, or anything too similar. ☺ The diagram should have at least six entities, twelve attributes and eight relations. 
            2: Needs more work before this can be used.
            3: Some small issues, but good enough to start creating a database.
            4: Perfect.
            weight: 4
