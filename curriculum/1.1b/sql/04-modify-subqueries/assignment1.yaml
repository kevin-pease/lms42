- Introduction: |
      In this assignment we will work on using SQL to modify the data in our database. Additionally, you will learn about subqueries.

- Before you begin:
    - SQL coding conventions: |
            When writing INSERT, UPDATE or DELETE statements please use the structure provided in the examples below. 
             
            ```sql
            INSERT INTO teacher (
                  first_name, 
                  t.last_name
            )
            VALUES (
                  'Timothy',
                  'Sealy'
            )

            UPDATE teacher
            SET email = 'sealy@sd42.nl'
            WHERE first_name = 'Timothy'
                  AND last_name = 'Sealy'

            DELETE 
            FROM teacher
            WHERE first_name = 'Timothy'
                  AND last_name = 'Sealy' 
            ```

            In addition to the previous rules:
            * You should put the INSERT, UPDATE and DELETE statement on a new line for clarity.
            * You may put multiple parameters below each other for clarity. Only do this when the list of parameters is long (more than 5) and be sure to indent them for clarity.


            For subqueries please use the following structure:
            ```sql
            SELECT t.first_name, t.last_name, 
            FROM teacher t
            WHERE t.id IN (
                  SELECT t2.id, COUNT(p.id) AS count_projects
                  FROM teacher t2
                  JOIN project p ON p.teacher_id = t.id
                  GROUP BY t.id
                  HAVING count_projects > 3
            )
            ```

            In addition to the previous rules:
            * You should put the sub query on a new line and indented for clarity.
            * The opening bracket '(' should be located on the line before the new line and the trailing bracket ')' should be locate on a new line as well.
            
- Assignment:
      - |
            Important notes:
            
            - For each of the questions below, we expect your answer to be a *single* query, unless stated otherwise.
            - Your queries should not contain literal ids or other information that was not given in the question. So when the assignment says to delete Frank, you can't do something like `delete from teachers where id=42` but you need to do something like `delete from teachers where name="Frank"`.

      - Insert data into the database:
            -     link: https://www.youtube.com/watch?v=nEndOUQFaOI
                  title: SQL Into - How to Copy Table Data with Select Into Statement
                  info: Discusses the SELECT INTO and INSERT INTO SELECT statements. Be sure to watch the entire clip for the trick at the end.
            -
                  weight: 1.5
                  text: |
                        Provide us with the SQL for the following queries:
                        1. Insert a new category in the database called 'Millennium'.
                        2. Insert a new actor in the database with the name Matthew McConaughey.
                        3. Insert a new movie into the database with name Interstellar, release year 2014, length 169 and rating PG-13. The rental duration 3 weeks, at the cost of 4.99 with a replacement value of 20.99. The movie contains the following special features "{Trailers,"Deleted Scenes"}" and description: "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.". For the full text supply an empty string.
                        4. Matthew McConaughey is an actor in Interstellar. Insert this link into the database. You are allowed to specify the literal id for Interstellar.
                        5. Add the film Interstellar in the inventory of the store located on '28 MySQL Boulevard'. You are allowed to specify the literal id for Interstellar.
                        6. Insert a new staff member in the database with name Bob Alisson and email bob.alisson@saxion.nl, who lives at 'Tromplaan 28' in Enschede, the Netherlands. Bob works in the store on 'MySQL Boulevard' and is the active manager of the (new) store located at 'Van Galenstraat 19' in Enschede in the Netherlands (no district or zip code). Insert the store as well. (two queries)
                        
                  0: No correct query.
                  1: Up to 2 queries are correct.
                  2: Between 2 and 4 queries are correct.
                  3: Between 4 and 6 queries are correct.
                  4: All (6) queries are correct.
            
      - Update data in the database: 
            text: |
                  Provide us with the SQL for the following queries:
                  1. Update staff member Jon Stephens email address to jon.stephens@saxion.nl.
                  2. All movies with id higher than 969 (excluding this id) have been moved to the store with `store_id` 2. Update the inventory accordingly. 
                  3. Update all customers with first or last name 'Morris' so that they are a customer of the store with `store_id` 1. 
                  4. Update all payments conducted on 14th of February 2007 so that everyone has a 50 cent discount.
                  
            0: No correct query.
            1: One correct query.
            2: Two correct query.
            3: Three correct query.
            4: All queries are correct.

      - Remove data from the database:
            text: |
                  Provide us with the SQL for the following queries::
                  1. Remove the category `New` for all the films that have it. Note that you should not remove the film but simply the category for the film. You are allowed to just use a literal id for the category this time. 
                  2. Remove the category with the name 'New'.
                  3. Delete all rentals that have been rented out on 30th of May on 2005. 
                  4. Delete all the movies with id 1000 from the inventory.

            0: No correct query.
            1: One correct query.
            2: Two correct query.
            3: Three correct query.
            4: All queries are correct.
      
      - Subqueries:
            text: |
                  Create SQL queries for the following questions:
                  1. Show all the customers that have the same first name as any of the staff members. Display the customer's first and last name along with their address and country. Sort the list by first name (ascending).
                  2. Delete all films from the inventory where the film has 'bug' in the title.
                  3. Find all films with a duration longer than the average film duration. Display the title and length of the film. Sort by duration (descending).
                  4. Find all films that have been released in the same year as the latest 'G' rated film where 'Sean Guiness' was an actor in. Display the title, release year and rental rate. Sort the results by rental rate from most expensive to the cheapest.
                  5. Find the 'binge watchers'. Display the first and last name of all the customers that have rented more films than average. Also include the number of rentals they have and sort the results from customer with most rented films to least rented. This requires the use of *nested subqueries*: subqueries within subqueries!
                  6. Relabel the millennium movies. Add the category 'Millennium' (in film_category) to all the films that have no category and have a release year greater than 1999. 

            0: No correct query.
            1: Up to 2 queries are correct.
            2: Between 2 and 4 queries are correct.
            3: Between 4 and 6 queries are correct.
            4: All (6) queries are correct.

      - Views:
            text: |
                  Create SQL queries for the following questions:
                  1. Create a view of ordered payments. The view should be named `v_ordered_payments`s and should return the first and last name of the customer who made the payment and staff who processed the payment. In addition the amount and payment date should be returned. Show the results by querying the view for all payments done by `Jon Stephens`. (two queries)
                  2. Create a view (`v_daily_income`) for the sum of all payments received in a day. Display the year, month, day and the total amount in payments received on that day. Query all the entries from the view. (two queries)
                  3. Create a view (`v_outstanding_rental`) that shows the films that have been rented but not yet returned. Include the rental date, film title, rental duration and customer id. Query the view so that you have a list of outstanding rental with a duration of more than 4 days. Display the the rental date, film title, the customer's first name, last name and email address. (two queries)
                  4. Create a view (`v_unpaid_rentals`) that returns the number of unpaid rentals for each day. Note that we are not looking for the rental with a payment amount of 0, but for rentals that have no payments associated with them. The results should be ordered by date. Use this view to query the list of customers that have rented a film on the day with the most unpaid rentals. 

            0: No correct query.
            1: One correct query.
            2: Two correct query.
            3: Three correct query.
            4: All queries are correct.