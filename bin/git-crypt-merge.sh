#!/bin/sh
echo Using git-crypt-merge
ancestor_decrypted="$1__decrypt"
current_decrypted="$2__decrypt"
other_decrypted="$3__decrypt"
cat $1 | git-crypt smudge > "${ancestor_decrypted}"
cat $2 | git-crypt smudge > "${current_decrypted}"
cat $3 | git-crypt smudge > "${other_decrypted}"

git merge-file -L "current branch" -L "ancestor branch" -L "other branch" "${current_decrypted}" "${ancestor_decrypted}" "${other_decrypted}"
exit_code=$?
cat "${current_decrypted}" | git-crypt clean > $2

rm "${other_decrypted}" "${ancestor_decrypted}" "${current_decrypted}"

exit $exit_code

