from ..app import db, app
from ..models.trial import TrialDay, TrialStudent
from ..utils import role_required, format_date
from ..email import send as send_email
from wtforms.fields.html5 import DateField
import flask
import sqlalchemy
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf



@app.route('/trial/<int:id>/<secret>', methods=['GET','POST'])
def trial_schedule(id, secret):
    student = TrialStudent.query.get(id)
    if not student or student.secret != secret:
        flask.flash("Sorry, that URL doesn't look right.")
        return flask.render_template('layout.html')

    if flask.request.method == "POST":
        if "cancel" in flask.request.form:
            date = student.trial_day_date
            student.trial_day_date = None
            db.session.commit()
            send_cancel_email(student, date)
        elif "date" in flask.request.form:
            date = flask.request.form["date"]
            day = TrialDay.query.get(date)
            if not day or len(day.students) >= day.slots:
                flask.flask("Sorry, that day is no longer available.")
            else:
                student.trial_day_date = date
                db.session.commit()
                send_scheduled_email(student)
                flask.flash("A confirmation email with further details has been sent.")
        else:
            flask.flash("Please select a date.")

    available = get_available_dates() if student.trial_day_date==None else None

    return flask.render_template('trial-schedule.html', student=student, available=available)




class StudentForm(flask_wtf.FlaskForm):
    first_name = wtf.StringField('First name', validators=[wtv.DataRequired()])
    last_name = wtf.StringField('Last name', validators=[])
    email = wtf.StringField('Email', validators=[wtv.Email(), wtv.DataRequired()])
    submit = wtf.SubmitField('Invite')


class DayForm(flask_wtf.FlaskForm):
    date = DateField('Date', validators=[wtv.DataRequired()])
    slots = wtf.IntegerField('Slot count', validators=[], default=2)
    submit = wtf.SubmitField('Save')


@app.route('/trial', methods=['GET','POST'])
@role_required('teacher')
def trial_list():
    student_form = StudentForm()
    if "email" in flask.request.form and student_form.validate_on_submit():
        try:
            student = TrialStudent()
            student_form.populate_obj(student)
            db.session.add(student)
            db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            db.session.rollback()
            flask.flash("Address already exists.")
        else:
            student_form = StudentForm()
            send_invite_email(student)


    day_form = DayForm()
    if "date" in flask.request.form and day_form.validate_on_submit():
        day = TrialDay.query.get(day_form.date.data) or TrialDay()
        day_form.populate_obj(day)
        db.session.add(day)
        db.session.commit()

    scheduled = list(get_scheduled_trials())
    
    scheduled.append({
        'date': "Unscheduled",
        'slots': "",
        'students': list(TrialStudent.query.filter_by(trial_day_date=None).order_by(TrialStudent.invitation_time)),
    })

    return flask.render_template('trial-list.html', student_form=student_form, day_form=day_form, scheduled=scheduled)


def get_available_dates():
    with db.engine.connect() as dbc:
        return [row["date"] for row in dbc.execute("""
                select td.date as date
                from trial_day td
                where
                    td.date>current_date and
                    (select count(*) from trial_student ts where ts.trial_day_date=td.date) < slots
                order by date
            """)]


def get_scheduled_trials():
    return TrialDay.query.order_by(TrialDay.date).options(sqlalchemy.orm.joinedload(TrialDay.students))


def send_invite_email(student):
    send_email(student.email, "Saxion invitation", f"""Hi {student.first_name},

You are warmly invited to participate in a trial day for the Associate degree Software Development at Saxion. Please refer to this link for more information and to schedule your visit:

{student.url}

We recommend that you make an appointment as soon as possible. We're looking forward to your visit!

Best regards,

Ad Software Development
Saxion

Any questions? Feel free to contact Frank van Viegen at f.c.vanviegen@saxion.nl.
""")



def send_scheduled_email(student):
    send_email(student.email, "Trial day scheduled", f"""Hi {student.first_name},
                
You scheduled your trial day for the Saxion Ad Software Development on {format_date(student.trial_day_date)}.

Instructions for your visit:
- We're located at Van Galenstraat 19, Enschede. Take the elevators to the 5th floor, and turn left into the long corridor. Our office is at the end, in room G5.27.
- Please try to arrive between 8:45 and 9:00 in the morning. The day ends between 16:00 and 17:00, although you're of course free to leave at any time.
- Bring your laptop and headphones.
- You may want to bring lunch, or you can buy some in the canteen.
- In case you need to cancel/reschedule, please do so as far in advance as you can, by visiting the scheduling page again: {student.url}

We're looking forward to your visit!

Best regards,

Ad Software Development
Saxion

Any questions? Feel free to contact Frank van Viegen at f.c.vanviegen@saxion.nl.
""")


def send_cancel_email(student, date):
    send_email(student.email, "Trial day cancelled", f"""Hi {student.first_name},
    
Your Saxion Ad Software Development trial day appointment on {format_date(date)} has been cancelled.

Best regards,

Ad Software Development
Saxion

Any questions? Feel free to contact Frank van Viegen at f.c.vanviegen@saxion.nl.
""")
